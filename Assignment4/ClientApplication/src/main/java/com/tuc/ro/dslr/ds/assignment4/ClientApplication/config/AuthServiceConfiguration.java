package com.tuc.ro.dslr.ds.assignment4.ClientApplication.config;

import com.tuc.ro.dslr.ds.assignment4.ClientApplication.client.AuthClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class AuthServiceConfiguration {
    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("auth.wsdl");
        return marshaller;
    }

    @Bean
    public AuthClient authClient(Jaxb2Marshaller marshaller) {
        AuthClient client = new AuthClient();
        client.setDefaultUri("http://localhost:8080/ws");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}