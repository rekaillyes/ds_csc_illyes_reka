package com.tuc.ro.dslr.ds.assignment4.ClientApplication.controller;

import auth.wsdl.RegisterUserRequest;
import auth.wsdl.RegisterUserResponse;
import auth.wsdl.UserDto;
import com.tuc.ro.dslr.ds.assignment4.ClientApplication.client.AuthClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RegisterController {

    private final AuthClient authClient;

    @Autowired
    RegisterController(AuthClient authClient){
        this.authClient=authClient;
    }

    @GetMapping("/register")
    public String registerPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "register";
    }

    @PostMapping()
    public String register(@ModelAttribute UserDto user) {
        user.setRole("USER");
        authClient.register(user);
        return "login";
    }
}
