package com.tuc.ro.dslr.ds.assignment4.ClientApplication.client;

import auth.wsdl.*;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class AuthClient extends WebServiceGatewaySupport {

    public RegisterUserResponse register(UserDto user) {

        RegisterUserRequest request = new RegisterUserRequest();
        request.setUser(user);

        RegisterUserResponse response = (RegisterUserResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/ws/auth", request,
                        new SoapActionCallback(
                                "http://ro.tuc.com/dslr/ds/assignment4/soap/RegisterUserRequest"));
        return response;
    }

    public LoginUserResponse login(LoginDto loginDto) {

        LoginUserRequest request = new LoginUserRequest();
        request.setLoginDto(loginDto);

        LoginUserResponse response = (LoginUserResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/ws/auth", request,
                        new SoapActionCallback(
                                "http://ro.tuc.com/dslr/ds/assignment4/soap/LoginUserResponse"));
        return response;
    }
}
