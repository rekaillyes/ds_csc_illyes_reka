package com.tuc.ro.dslr.ds.assignment4.ClientApplication.config;

import auth.wsdl.LoginDto;
import auth.wsdl.LoginUserResponse;
import auth.wsdl.UserDto;
import com.tuc.ro.dslr.ds.assignment4.ClientApplication.client.AuthClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.management.relation.Role;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final AuthClient authClient;

    public CustomAuthenticationProvider(AuthClient authClient) {
        this.authClient = authClient;
    }

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();

        LoginDto loginDto = new LoginDto();
        loginDto.setUsername(username);
        loginDto.setPassword(password);


        LoginUserResponse authenticationResponse = authClient.login(loginDto);

        UserDto loggedInUser = authenticationResponse.getUser();

        if (Objects.isNull(loggedInUser)) {
            throw new BadCredentialsException("Username sau parola gresita");
        }

        GrantedAuthority role = new SimpleGrantedAuthority(loggedInUser.getRole());
        List<GrantedAuthority> authoritiesList = new ArrayList<>();
        authoritiesList.add(role);
        Collection<? extends GrantedAuthority> authorities = authoritiesList;

        return new UsernamePasswordAuthenticationToken(username, password, authorities);
    }

    public boolean supports(Class<?> aClass) {
        return true;
    }
}