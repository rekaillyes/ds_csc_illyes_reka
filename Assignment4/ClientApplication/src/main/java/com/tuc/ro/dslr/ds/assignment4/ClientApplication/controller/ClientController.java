package com.tuc.ro.dslr.ds.assignment4.ClientApplication.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/client")
@Controller
public class ClientController {

    @GetMapping
    public String getClientPage() {
        return "client";
    }
}
