package com.tuc.ro.dslr.ds.assignment4.OnlineTracking.service.impl;

import com.tuc.ro.dslr.ds.assignment4.OnlineTracking.model.User;
import com.tuc.ro.dslr.ds.assignment4.OnlineTracking.model.mapper.UserMapper;
import com.tuc.ro.dslr.ds.assignment4.OnlineTracking.repository.UserRepository;
import com.tuc.ro.dslr.ds.assignment4.OnlineTracking.service.UserService;
import com.tuc.ro.dslr.ds.assignment4.soap.LoginDto;
import com.tuc.ro.dslr.ds.assignment4.soap.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public UserDto register(UserDto user) {
        User registeredUser = userMapper.convert(user);
        return userMapper.convert(userRepository.save(registeredUser));
    }

    @Override
    public UserDto login(LoginDto loginDto) {
        return userMapper.convert(userRepository.findByUsernameAndPassword(loginDto.getUsername(), loginDto.getPassword()));
    }
}
