package com.tuc.ro.dslr.ds.assignment4.OnlineTracking.model.mapper;

import com.tuc.ro.dslr.ds.assignment4.OnlineTracking.model.User;
import com.tuc.ro.dslr.ds.assignment4.soap.UserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User convert(UserDto dto);
    UserDto convert(User user);
}
