package com.tuc.ro.dslr.ds.assignment4.OnlineTracking.endpoint;

import com.tuc.ro.dslr.ds.assignment4.OnlineTracking.service.UserService;
import com.tuc.ro.dslr.ds.assignment4.soap.LoginUserRequest;
import com.tuc.ro.dslr.ds.assignment4.soap.LoginUserResponse;
import com.tuc.ro.dslr.ds.assignment4.soap.RegisterUserRequest;
import com.tuc.ro.dslr.ds.assignment4.soap.RegisterUserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class LoginEndpoint {
    private static final String NAMESPACE_URI = "http://ro.tuc.com/dslr/ds/assignment4/soap";

    private final UserService userService;

    @Autowired
    public LoginEndpoint(UserService userService) {
        this.userService = userService;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "loginUserRequest")
    @ResponsePayload
    public LoginUserResponse loginUserResponse(@RequestPayload LoginUserRequest request) {
        LoginUserResponse response = new LoginUserResponse();
        response.setUser(userService.login(request.getLoginDto()));

        return response;
    }
}
