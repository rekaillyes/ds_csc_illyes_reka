package com.tuc.ro.dslr.ds.assignment4.OnlineTracking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineTrackingApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineTrackingApplication.class, args);
	}

}

