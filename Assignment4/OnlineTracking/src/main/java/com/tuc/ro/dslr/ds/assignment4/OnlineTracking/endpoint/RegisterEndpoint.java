package com.tuc.ro.dslr.ds.assignment4.OnlineTracking.endpoint;

import com.tuc.ro.dslr.ds.assignment4.OnlineTracking.service.UserService;
import com.tuc.ro.dslr.ds.assignment4.soap.RegisterUserRequest;
import com.tuc.ro.dslr.ds.assignment4.soap.RegisterUserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class RegisterEndpoint {

    private static final String NAMESPACE_URI = "http://ro.tuc.com/dslr/ds/assignment4/soap";

    private final UserService userService;

    @Autowired
    public RegisterEndpoint(UserService userService) {
        this.userService = userService;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "registerUserRequest")
    @ResponsePayload
    public RegisterUserResponse registerUserResponse(@RequestPayload RegisterUserRequest request) {
        RegisterUserResponse response = new RegisterUserResponse();
        response.setUser(userService.register(request.getUser()));

        return response;
    }
}
