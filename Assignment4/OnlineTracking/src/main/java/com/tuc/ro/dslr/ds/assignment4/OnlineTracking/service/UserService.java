package com.tuc.ro.dslr.ds.assignment4.OnlineTracking.service;

import com.tuc.ro.dslr.ds.assignment4.soap.LoginDto;
import com.tuc.ro.dslr.ds.assignment4.soap.UserDto;

public interface UserService {
    UserDto register(UserDto user);

    UserDto login(LoginDto loginDto);
}
