package com.tuc.ro.dslr.ds.assignment4.OnlineTracking.repository;

import com.tuc.ro.dslr.ds.assignment4.OnlineTracking.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, String> {
    User findByUsername(String username);

    List<User> findAllByRole(String role);

    User findByUsernameAndPassword(String username, String password);
}
