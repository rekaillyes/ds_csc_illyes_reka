<%--
  Created by IntelliJ IDEA.
  User: Sintean Anca
  Date: 10/27/2018
  Time: 4:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.css'/>">
    <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-grid.css'/>">
    <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-reboot.css'/>">
    <script src="<c:url value='/resources/js/jquery.js'/>"></script>
    <script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Admin Page</title>
</head>
<body>
<div class="container ">
    <div class="jumbotron" style="color: #1b6d85;  margin-top:10px; height: 100%; box-shadow: 0px 4px 4px 0px #FFFFFF;">
        <div style=" float:left; margin-top:6%;" class="well-searchbox">
            <form class="form-horizontal" role="form" action="/flight" method="get">
                <div class="form-group">
                    <div class="col-md-8">
                        <left><h2>Search</h2></left>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Departure</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" placeholder="Departure City" name="departureCity" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Departure</label>
                    <div class="col-md-8">
                        <input type="date" class="form-control" placeholder="Departure Date" name="departureDate" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Arrival</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" placeholder="Arrival City" name="arrivalCity" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Arrival</label>
                    <div class="col-md-8">
                        <input type="date" class="form-control" placeholder="Departure Date" name="arrivalDate" required/>
                    </div>
                </div>
                <div class="col-sm-offset-4 col-sm-5">
                    <button type="submit" class="btn btn-success">Search</button>
                </div>
                <button type="button" onclick="window.location.href='/admin'" class="btn btn-success">All</button>
            </form>
        </div>
        <div style="margin-top:5%;float:right" class="right-container">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Airplane</th>
                    <th>Departure City</th>
                    <th>Departure Date</th>
                    <th>Arrival City</th>
                    <th>Arrival Date</th>
                </tr>
                </thead>
                <c:if test="${!empty error}">${error}</c:if>
                <c:forEach var="flight" items="${flights}">
                    <tbody>
                    <tr>
                        <td>${flight.airplane}</td>
                        <td>${flight.departureCity}</td>
                        <td>${flight.departureDate}</td>
                        <td>${flight.arrivalCity}</td>
                        <td>${flight.arrivalDate}</td>
                        <td><a href="/flight?update=1&flightNumber=${flight.flightNumber}">Update</a></td>
                        <td><a href="/flight?delete=1&flightNumber=${flight.flightNumber}">Del</a></td>
                    </tr>
                    </tbody>
                </c:forEach>
            </table>
            <button class="btn btn-primary btn-success" data-toggle="modal" data-target="#myModalForCreate">
                Add new
            </button>
            <c:if test="${!empty flight}">
                <div class="jumbotron">
                    <form class="form-horizontal" role="form" action="/flight" method="post">
                        <input type="hidden" name="flightNumber" value="${flight.flightNumber}">
                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="${flight.airplane}" name="airplane" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="${flight.departureCity}" name="departureCity" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="date" class="form-control" placeholder="${flight.departureDate}" name="departureDate" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="${flight.arrivalCity}" name="arrivalCity" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="date" class="form-control" placeholder="${flight.arrivalDate}" name="arrivalDate" required/>
                            </div>
                        </div>
                        <div class="col-sm-offset-4 col-sm-5">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </form>
                </div>
            </c:if>
        </div>
    </div>


    <%--Modal model --%>
    <div class="modal fade" id="myModalForCreate" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <h2>Add a new flight</h2>
                </div>
                <form action="/flight" method="post">
                    <div class="form-group">
                        <label for="airplane">Airplane type:</label>
                        <input type="text" class="form-control" id="airplane" name="airplane">
                    </div>
                    <div class="form-group">
                        <label for="departureCity">Departure city:</label>
                        <input type="text" class="form-control" id="departureCity" name="departureCity">
                    </div>
                    <div class="form-group">
                        <label for="departureDate">Departure date:</label>
                        <input type="date" class="form-control" id="departureDate" name="departureDate">
                    </div>
                    <div class="form-group">
                        <label for="arrivalCity">Arrival city:</label>
                        <input type="text" class="form-control" id="arrivalCity" name="arrivalCity">
                    </div>
                    <div class="form-group">
                        <label for="arrivalDate">Arrival date:</label>
                        <input type="date" class="form-control" id="arrivalDate" name="arrivalDate">
                    </div>

                    <div class="form-group">
                        <label for="arrivalCity">User:</label>
                        <input type="text" class="form-control" id="users" name="users">
                    </div>
                    <input style = "margin-left:15px"  type="submit" class="btn btn-default btn-success"/>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <%--Modal model for update--%>
    <div class="modal fade" id="myModalForUpdate" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <h2>Add a new flight</h2>
                </div>
                <form action="/flight" method="post">
                    <div class="form-group">
                        <label for="airplane">Airplane type:</label>
                        <input type="text" class="form-control" id="airplane" name="airplane">
                    </div>
                    <div class="form-group">
                        <label for="departureCity">Departure city:</label>
                        <input type="text" class="form-control" id="departureCity" name="departureCity">
                    </div>
                    <div class="form-group">
                        <label for="departureDate">Departure date:</label>
                        <input type="date" class="form-control" id="departureDate" name="departureDate">
                    </div>
                    <div class="form-group">
                        <label for="arrivalCity">Arrival city:</label>
                        <input type="text" class="form-control" id="arrivalCity" name="arrivalCity">
                    </div>
                    <div class="form-group">
                        <label for="arrivalDate">Arrival date:</label>
                        <input type="date" class="form-control" id="arrivalDate" name="arrivalDate">
                    </div>
                    <input style = "margin-left:15px"  type="submit" class="btn btn-default btn-success"/>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <%--Model for update ends here--%>




</div>
</body>
</html>
