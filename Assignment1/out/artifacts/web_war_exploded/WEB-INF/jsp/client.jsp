<%--
  Created by IntelliJ IDEA.
  User: Sintean Anca
  Date: 10/28/2018
  Time: 7:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Client Page</title>
    <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.css'/>">
    <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-grid.css'/>">
    <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-reboot.css'/>">
    <script src="<c:url value='/resources/js/jquery.js'/>"></script>
    <script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
</head>
<body>


<div class="container ">
    <div class="jumbotron" style="color: #1b6d85;  margin-top:100px; height: 90%; box-shadow: 0px 4px 4px 0px #FFFFFF;">
        <div style="margin-top:5%" class="container">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Airplane</th>
                    <th>Departure City</th>
                    <th>Departure Date</th>
                    <th>Arrival City</th>
                    <th>Arrival Date</th>
                </tr>
                </thead>
                <c:if test="${!empty error}">${error}</c:if>
                <c:forEach var="flight" items="${flights}">
                    <tbody>
                    <tr>
                        <td>${flight.airplane}</td>
                        <td><a href="/timezone?flightNumber=${flight.flightNumber}">${flight.departureCity}</a></td>
                        <td>${flight.departureDate}</td>
                        <td><a href="/timezone?flightNumber=${flight.flightNumber}">${flight.arrivalCity}</a></td>
                        <td>${flight.arrivalDate}</td>
                    </tr>
                    </tbody>
                </c:forEach>
                <c:if test="${!empty localtime}">
                    <h3>Localtime for departure city: ${localtime} </h3>
                </c:if>

            </table>
        </div>
    </div>

</div>
</body>
</html>
