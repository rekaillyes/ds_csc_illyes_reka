package business;

import org.hibernate.cfg.Configuration;
import persistence.dao.UserDAO;
import persistence.dao.impl.UserDAOImpl;
import persistence.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {

    private UserDAO userDAO;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        userDAO = new UserDAOImpl(new Configuration().configure().buildSessionFactory());

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        req.getRequestDispatcher("link.html").include(req, resp);

        String username = req.getParameter("username");
        String password = req.getParameter("password");

        User user = userDAO.findByUsername(username);

        if (password.equals(user.getPassword())) {
            HttpSession session = req.getSession();
            session.setAttribute("username", username);
            if (user.getRole().equals("admin")) {
                resp.sendRedirect("/admin");
            } else {
                resp.sendRedirect("/client");
            }
        } else {
            out.print("Sorry, username or password error!");
            req.getRequestDispatcher("login.html").include(req, resp);
        }
        out.close();
    }


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }
}
