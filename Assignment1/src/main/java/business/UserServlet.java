package business;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import persistence.dao.UserDAO;
import persistence.dao.impl.FlightDAOImpl;
import persistence.dao.impl.UserDAOImpl;
import persistence.entities.Flight;
import persistence.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class UserServlet extends HttpServlet {

    private FlightDAOImpl flightDao;
    private static final String CLIENT_URL = "/WEB-INF/jsp/client.jsp";

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        flightDao = new FlightDAOImpl(new Configuration().configure().buildSessionFactory());
        super.service(req, resp);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if(req.getParameter("localtime") != null){
            req.setAttribute("localtime", req.getParameter("localtime"));
        }
        List<Flight> flights = flightDao.getAllFlights();
        req.setAttribute("flights", flights);
        req.getRequestDispatcher(CLIENT_URL).forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

    }
}
