package business;

import org.hibernate.cfg.Configuration;
import persistence.dao.FlightDAO;
import persistence.dao.UserDAO;
import persistence.dao.impl.FlightDAOImpl;
import persistence.dao.impl.UserDAOImpl;
import persistence.entities.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class AdminServlet extends HttpServlet {

    private UserDAO userDao;
    private FlightDAO flightDao;
    private static final String ADMIN_URL = "/WEB-INF/jsp/admin.jsp";

    protected void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        userDao = new UserDAOImpl(new Configuration().configure().buildSessionFactory());
        flightDao = new FlightDAOImpl(new Configuration().configure().buildSessionFactory());
        super.service(req, resp);
    }


    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if(req.getParameter("flightNumber") != null && req.getParameter("update") != null){
            Flight flight = flightDao.getByFlightNumber(Integer.parseInt(req.getParameter("flightNumber")));
             req.setAttribute("flight", flight);
        }
        List<Flight> flights = flightDao.getAllFlights();
        req.setAttribute("flights", flights);
        req.getRequestDispatcher(ADMIN_URL).forward(req, resp);

    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

    }
}
