package business;

import org.hibernate.cfg.Configuration;
import persistence.dao.FlightDAO;
import persistence.dao.UserDAO;
import persistence.dao.impl.FlightDAOImpl;
import persistence.dao.impl.UserDAOImpl;
import persistence.entities.Flight;
import persistence.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FlightServlet extends HttpServlet {

    private FlightDAO flightDao;
    private UserDAO userDao;

    private static final String CLIENT_URL = "/WEB-INF/jsp/client.jsp";
    private static final String ADMIN_URL = "/WEB-INF/jsp/admin.jsp";
    private static final String ERROR_URL = "/WEB-INF/jsp/403.jsp";

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        flightDao = new FlightDAOImpl(new Configuration().configure().buildSessionFactory());
        userDao = new UserDAOImpl(new Configuration().configure().buildSessionFactory());
        super.service(req, resp);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if (req.getParameter("departureCity") != null) {
            List<Flight> flights = null;
            try {
                flights = flightDao.getByDetails(buildFlight(req));
                req.setAttribute("flights", flights);
            } catch (ParseException e) {
                req.setAttribute("error", e.getMessage());
            }
            redirectByRole(req, resp);
        } else if (req.getParameter("flightNumber") != null && req.getParameter("delete") != null) {
            int flightNumber = Integer.parseInt(req.getParameter("flightNumber"));
            flightDao.deleteFlight(flightNumber);
            redirectByRole(req, resp);
        } else if (req.getParameter("flightNumber") != null && req.getParameter("update") != null) {
            int flightNumber = Integer.parseInt(req.getParameter("flightNumber"));
            redirectForUpdate(req, resp);
        } else {
            redirectByRole(req, resp);
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        try {
            Flight flight = buildFlight(req);
            String flightNumber = req.getParameter("flightNumber");
            if (flightNumber == null) {
                flight.setAirplane(req.getParameter("airplane"));
                createFlight(flight);
                redirectByRole(req, resp);
            } else {
                flight.setFlightNumber(Integer.parseInt(flightNumber));
                updateFlight(flight);
                redirectByRole(req, resp);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void redirectByRole(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User currentUser = userDao.findByUsername((String) req.getSession().getAttribute("username"));
        boolean isClient = currentUser.getRole().equals("user");
        boolean isAdmin = currentUser.getRole().equals("admin");

        if (isClient) {
            req.getRequestDispatcher(CLIENT_URL).forward(req, resp);
        } else if (isAdmin) {
            req.getRequestDispatcher(ADMIN_URL).forward(req, resp);
        } else {
            req.getRequestDispatcher(ERROR_URL).forward(req, resp);
        }
    }

    private void redirectForUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        User currentUser = userDao.findByUsername((String) req.getSession().getAttribute("username"));
        boolean isClient = currentUser.getRole().equals("user");
        boolean isAdmin = currentUser.getRole().equals("admin");

        if (isClient) {
            resp.sendRedirect("/client");
        } else if (isAdmin) {
            resp.sendRedirect("/admin?flightNumber=" + req.getParameter("flightNumber") + "&update=1");
        } else {
            req.getRequestDispatcher(ERROR_URL).forward(req, resp);
        }
    }


    private void createFlight(Flight flight) {
        flightDao.addFlight(flight);
    }

    private void updateFlight(Flight flight) {
        flightDao.updateFlight(flight);
    }


    private Flight buildFlight(HttpServletRequest req) throws ParseException {
        Flight flight = new Flight();

        String departureCity = req.getParameter("departureCity");
        String departure_date = req.getParameter("departureDate");
        String arrivalCity = req.getParameter("arrivalCity");
        String arrival_date = req.getParameter("arrivalDate");

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date departureDate = format.parse(departure_date);
        java.sql.Date departureSqlDate = new java.sql.Date(departureDate.getTime());
        Date arrivalDate = format.parse(arrival_date);
        java.sql.Date arrivalSqlDate = new java.sql.Date(arrivalDate.getTime());

        flight.setDepartureCity(departureCity);
        flight.setDepartureDate(departureSqlDate);
        flight.setArrivalCity(arrivalCity);
        flight.setArrivalDate(arrivalSqlDate);

        return flight;
    }
}