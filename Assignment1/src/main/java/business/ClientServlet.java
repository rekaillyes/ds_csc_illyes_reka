package business;

import org.hibernate.cfg.Configuration;
import persistence.dao.FlightDAO;
import persistence.dao.UserDAO;
import persistence.dao.impl.FlightDAOImpl;
import persistence.dao.impl.UserDAOImpl;
import persistence.entities.Flight;
import persistence.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ClientServlet extends HttpServlet {

    private FlightDAO flightDao;
    private UserDAO userDao;
    private static final String CLIENT_URL = "/WEB-INF/jsp/client.jsp";

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        flightDao = new FlightDAOImpl(new Configuration().configure().buildSessionFactory());
        userDao = new UserDAOImpl(new Configuration().configure().buildSessionFactory());
        super.service(req, resp);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if(req.getParameter("localtime") != null){
            req.setAttribute("localtime", req.getParameter("localtime"));
        }
        String username = (String) req.getSession().getAttribute("username");
        User currentUser = userDao.findByUsername(username);
        List<Flight> flights = new ArrayList<Flight>(currentUser.getFlights());
        req.setAttribute("flights", flights);
        req.getRequestDispatcher(CLIENT_URL).forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

    }
}