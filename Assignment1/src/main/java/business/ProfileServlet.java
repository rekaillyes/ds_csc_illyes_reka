package business;

import org.hibernate.cfg.Configuration;
import persistence.dao.UserDAO;
import persistence.dao.impl.UserDAOImpl;
import persistence.entities.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class ProfileServlet extends HttpServlet {

    private UserDAO userDAO;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        userDAO = new UserDAOImpl(new Configuration().configure().buildSessionFactory());

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        request.getRequestDispatcher("link.html").include(request, response);

        HttpSession session = request.getSession(false);
        if (session != null) {
            String username = (String) session.getAttribute("username");
            User currentUser = userDAO.findByUsername(username);
            if(currentUser.getRole().equals("admin")){
                out.print("Hello, admin: " + currentUser.getName() + " Welcome to Profile");
            }else{
                out.print("Hello, user:  " + currentUser.getName() + " Welcome to Profile");
            }

        } else {
            out.print("Please login first");
            request.getRequestDispatcher("login.html").include(request, response);
        }
        out.close();
    }
}
