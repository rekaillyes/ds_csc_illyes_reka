package business;

import communication.TimeZoneService;
import org.hibernate.cfg.Configuration;
import persistence.dao.CityDAO;
import persistence.dao.FlightDAO;
import persistence.dao.impl.CityDAOImpl;
import persistence.dao.impl.FlightDAOImpl;
import persistence.entities.City;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class TimeZoneServlet extends HttpServlet {

    private CityDAO cityDao;
    private FlightDAO flightDao;
    private TimeZoneService timezoneService;
    private static final String ADMIN_URL = "/WEB-INF/jsp/admin.jsp";

    protected void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        cityDao = new CityDAOImpl(new Configuration().configure().buildSessionFactory());
        flightDao = new FlightDAOImpl(new Configuration().configure().buildSessionFactory());
        timezoneService = new TimeZoneService();
        super.service(req, resp);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        if(req.getParameter("flightNumber") == null){
            resp.sendRedirect("/client");
        }
        int flightNumber = Integer.parseInt(req.getParameter("flightNumber"));
        String departureCity = flightDao.getByFlightNumber(flightNumber).getDepartureCity();
        String arrivalCity = flightDao.getByFlightNumber(flightNumber).getArrivalCity();
        City departureCityCoord = cityDao.getByCity(departureCity);
        String departureLocaltime = timezoneService.getTimezone(departureCityCoord);
        req.setAttribute("departureLocaltime", departureLocaltime);
        resp.sendRedirect("/client?departureLocaltime="+departureLocaltime);
        City arrivalCityCoord = cityDao.getByCity(arrivalCity);
        String arrivalLocaltime = timezoneService.getTimezone(arrivalCityCoord);
        req.setAttribute("arrivalLocaltime", arrivalLocaltime);
        resp.sendRedirect("/client?arrivalLocaltime="+arrivalLocaltime);

    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

    }

}