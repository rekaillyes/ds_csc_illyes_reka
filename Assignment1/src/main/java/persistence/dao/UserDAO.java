package persistence.dao;

import persistence.entities.User;

import java.util.List;


public interface UserDAO {

    public User findById(int id);
    public User addUser(User user);
    public User findByUsername(String username);

}
