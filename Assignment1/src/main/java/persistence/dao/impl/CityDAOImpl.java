package persistence.dao.impl;

import org.hibernate.*;
import persistence.dao.CityDAO;
import persistence.entities.City;

import java.util.ArrayList;
import java.util.List;

public class CityDAOImpl implements CityDAO {
    private SessionFactory factory;

    public CityDAOImpl(SessionFactory factory){
        this.factory = factory;
    }

    @Override
    public City getByCity(String city){

        List<City> cities = new ArrayList<City>();
        Session session = factory.openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE city =:city");
            query.setParameter("city", city);
            cities = query.list();
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
        return cities.get(0);
    }
}
