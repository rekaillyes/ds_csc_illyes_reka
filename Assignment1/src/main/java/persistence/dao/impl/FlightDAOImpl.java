package persistence.dao.impl;

import org.hibernate.*;
import persistence.dao.FlightDAO;
import persistence.entities.Flight;
import persistence.entities.User;

import java.util.ArrayList;
import java.util.List;

public class FlightDAOImpl implements FlightDAO{

    private SessionFactory factory;

    public FlightDAOImpl(SessionFactory factory){
        this.factory = factory;
    }

    @Override
    public Flight addFlight(Flight flight){

        Session session = factory.openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(flight);
            tx.commit();
        }catch (HibernateException e){
            if(tx != null){
                tx.rollback();
            }
        }finally {
            session.close();
        }
        return flight;
    }

    @Override
    public List<Flight> getAllFlights(){

        List<Flight> flights = new ArrayList<Flight>();
        Session session = factory.openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight");
            flights = query.list();
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
        return flights;
    }

    @Override
    public List<Flight> getByDetails(Flight flight){

        List<Flight> flights = new ArrayList<Flight>();
        Session session = factory.openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE departureCity =:departureCity AND departureDate =:departureDate AND arrivalCity =:arrivalCity AND arrivalDate =:arrivalDate");
            query.setParameter("departureCity", flight.getDepartureCity());
            query.setParameter("departureDate", flight.getDepartureDate());
            query.setParameter("arrivalCity", flight.getArrivalCity());
            query.setParameter("arrivalDate", flight.getArrivalDate());
            flights = query.list();
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
        return flights;
    }

    @Override
    public Flight getByFlightNumber(long flightNumber){

        List<Flight> flights = new ArrayList<Flight>();
        Session session = factory.openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE flightNumber =:flightNumber");
            query.setParameter("flightNumber", flightNumber);
            flights = query.list();
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
        return flights.get(0);
    }

    @Override
    public void updateFlight(Flight flight) {

        Session session = factory.openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("UPDATE Flight SET departureCity =:departureCity, " +
                    "departureDate =:departureDate, arrivalCity =:arrivalCity " +
                    ", arrivalDate =:arrivalDate  WHERE flightNumber =:flightNumber");
            query.setParameter("flightNumber", flight.getFlightNumber());
            query.setParameter("departureCity", flight.getDepartureCity());
            query.setParameter("departureDate", flight.getDepartureDate());
            query.setParameter("arrivalCity", flight.getArrivalCity());
            query.setParameter("arrivalDate", flight.getArrivalDate());
            query.executeUpdate();
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
    }

    @Override
    public void deleteFlight(int flightNumber) {

        Session session = factory.openSession();
        Transaction tx = null;

        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("DELETE Flight where flightNumber =: flightNumber");
            query.setParameter("flightNumber", flightNumber);
            query.executeUpdate();
            tx.commit();
        }catch (HibernateException e){

        }finally {
            session.close();
        }
    }



}
