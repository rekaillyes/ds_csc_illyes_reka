package persistence.dao;

import persistence.entities.City;


public interface CityDAO {
    public City getByCity(String city);
}
