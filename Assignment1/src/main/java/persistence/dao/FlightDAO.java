package persistence.dao;

import persistence.entities.Flight;
import persistence.entities.User;

import java.util.List;


public interface FlightDAO {

    public Flight addFlight(Flight flight);
    public List<Flight> getAllFlights();
    public List<Flight> getByDetails(Flight flight);
    public Flight getByFlightNumber(long flightNumber);
    public void updateFlight(Flight flight);
    public void deleteFlight(int flight_number);

}
