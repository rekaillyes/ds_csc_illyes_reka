package communication;

import persistence.entities.City;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import persistence.entities.Timezone;

public class TimeZoneService {

    public String getTimezone(City city){

        String url = "http://www.new.earthtools.org/timezone/" + city.getLatitude() + "/" + city.getLongitude();
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Timezone> response = restTemplate.getForEntity(url, Timezone.class);
        return response.getBody().getLocaltime();
    }
}
