package com.rmiinterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote {
	public int fabricationYear(int year) throws RemoteException;
	public int engineSize(int engineSize) throws RemoteException;
	public Double price(Double price) throws RemoteException;
	public int taxCalculator(int engineSize)throws RemoteException;
}