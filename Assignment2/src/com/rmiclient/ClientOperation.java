package com.rmiclient;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.*;

import com.rmiinterface.RMIInterface;

public class ClientOperation {
	private static RMIInterface car_Details;

	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		
		car_Details = (RMIInterface) Naming.lookup("//localhost/MyServer");
//		String fabricationYear = JOptionPane.showInputDialog("What is the fabrication year?");
//		String engineSize = JOptionPane.showInputDialog("What is the engine size?");
//		String price = JOptionPane.showInputDialog("What is the price?");

		JTextField fabricationYear = new JTextField(5);
		JTextField engineSize = new JTextField(5);
		JTextField price = new JTextField(5);

		JPanel myPanel = new JPanel();
		myPanel.add(new JLabel("In what year was the car made?"));
		myPanel.add(fabricationYear);

		myPanel.add(new JLabel("What is the engines size (cm^3)?"));
		myPanel.add(engineSize);

		myPanel.add(Box.createVerticalStrut(15)); // a spacer
		myPanel.add(new JLabel("How much does it cost?"));
		myPanel.add(price);

		JOptionPane.showConfirmDialog(null, myPanel,
				"Please enter the values", JOptionPane.OK_CANCEL_OPTION);

		int fabricationYearInt = car_Details.fabricationYear(Integer.parseInt(fabricationYear.getText()));
		int engineSizeInt = car_Details.engineSize(Integer.parseInt(engineSize.getText()));
		Double priceDouble = car_Details.price(Double.parseDouble(price.getText()));

		int tax = car_Details.taxCalculator(engineSizeInt);
		JOptionPane.showMessageDialog(null, "Your tax is: "+tax+" RON");
	}


}
