package com.rmiserver;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.rmiinterface.RMIInterface;

public class ServerOperation extends UnicastRemoteObject implements RMIInterface {
	private static final long serialVersionUID = 1L;

	protected ServerOperation() throws RemoteException {
		super();
	}

	@Override
	public int fabricationYear(int year) throws RemoteException{
		System.err.println("Fabrication year is: " + year);
		return year;
	}

	@Override
	public int engineSize(int engineSize) throws RemoteException {
		System.err.println("Engine size is: " + engineSize);
		return engineSize;
	}

	@Override
	public Double price(Double price) throws RemoteException {
		System.err.println("Price is: " + price);
		return price;
	}

	@Override
	public int taxCalculator(int engineSize) throws RemoteException {
		// Tax formula:
		int sum = engineSize < 1600 ? 8
				: engineSize <= 2000 ? 18
				: engineSize <= 2600 ? 72
				: engineSize <= 3001 ? 144 : 290;
		return engineSize / 200 * sum;
	}

	public static void main(String[] args){
		try {
			Naming.rebind("//localhost/MyServer", new ServerOperation());            
            System.err.println("Server ready");
            
        } catch (Exception e) {
        	System.err.println("Server exception: " + e.toString());
          e.printStackTrace();
        }
	}
}
