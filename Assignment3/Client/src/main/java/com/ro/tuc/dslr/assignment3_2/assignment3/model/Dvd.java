package com.ro.tuc.dslr.assignment3_2.assignment3.model;

import java.io.Serializable;

public class Dvd implements Serializable {

    private String title;
    private int year;
    private double price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    // Applying the Builder Design Pattern

    public Dvd withTitle(String title) {
        this.title = title;
        return this;
    }

    public Dvd withYear(int year) {
        this.year = year;
        return this;
    }

    public Dvd withPrice(double price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString(){
        return "Title : " + title + " Year : " + year + " Price : "+ price;
    }
}
