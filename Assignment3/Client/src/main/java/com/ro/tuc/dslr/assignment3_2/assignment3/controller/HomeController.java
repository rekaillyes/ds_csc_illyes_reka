package com.ro.tuc.dslr.assignment3_2.assignment3.controller;

import com.ro.tuc.dslr.assignment3_2.assignment3.model.Dvd;
import com.ro.tuc.dslr.assignment3_2.assignment3.service.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Controller
public class HomeController {

    private final String HOMEPAGE = "index";

    @Autowired
    private Publisher publisher;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getHomeLandPage(Model model) {
        model.addAttribute("dvd", new Dvd());
        return HOMEPAGE;
    }

    @PostMapping("/dvd")
    public String asd(@ModelAttribute Dvd dvd, Model model) {
        try {
            publisher.publish(dvd);
        } catch (Exception e) {
            model.addAttribute("message", "Dvd-ul nu a fost adaugat");
            return HOMEPAGE;
        }
        model.addAttribute("message", "Dvd-ul a fost adaugat cu succes");
        return HOMEPAGE;
    }
}
