package com.ro.tuc.dslr.assignment3_2.assignment3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DvdStoreApplication {

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(DvdStoreApplication.class, args);
	}
}
