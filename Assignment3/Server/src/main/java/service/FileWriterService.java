package service;

import java.io.*;

public class FileWriterService {

    File file;

    public FileWriterService(){
        file = new File("C:\\Users\\Reka\\Documents\\CSC\\II\\SD\\Tema 3\\Server\\output201.txt");
    }

    public void writeInFile(String message){

        try {
            java.io.FileWriter fw = new java.io.FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            // write in file
            bw.write(message);
            // close connection
            bw.close();
        } catch (FileNotFoundException e) {
            System.out.println("Problem writing in file");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}