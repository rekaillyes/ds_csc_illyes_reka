package rabbitLauncher;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitLauncher {

    private static final String EXCHANGE_NAME = "dvds";
    private static final String QUEUE_NAME_MAILSENDER = "mailsenderQ";
    private static final String QUEUE_NAME_FILEWRITER = "filewriterQ";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setPort(8080);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        //set the queue for mailsender consumer
        channel.queueDeclare(QUEUE_NAME_MAILSENDER, true, false, false, null);
        channel.queueBind(QUEUE_NAME_MAILSENDER, EXCHANGE_NAME, "");

        //set the queue for filewriter consumer
        channel.queueDeclare(QUEUE_NAME_FILEWRITER, true, false, false, null);
        channel.queueBind(QUEUE_NAME_FILEWRITER, EXCHANGE_NAME, "");
    }
}
